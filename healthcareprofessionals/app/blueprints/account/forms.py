import json
import os

from wtforms_alchemy import Unique, ModelForm

from app.models import User
from flask import url_for
from flask_uploads import UploadSet, IMAGES
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import ValidationError
from wtforms.fields import BooleanField, PasswordField, StringField, SubmitField, IntegerField, SelectField, \
    TextAreaField
from wtforms.fields.html5 import EmailField, DateField
from wtforms.validators import Email, EqualTo, InputRequired, Length, Optional
from wtforms_alchemy import model_form_factory

from app.utils import countries, states, professions, basedir, get_langs

BaseModelForm = model_form_factory(FlaskForm)

images = UploadSet('images', IMAGES)


class LoginForm(FlaskForm):
    email = EmailField('Email', validators=[InputRequired(), Length(1, 64), Email()])
    password = PasswordField('Password', validators=[InputRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Log in')


class RegistrationForm(BaseModelForm):
    first_name = StringField('First name', validators=[InputRequired()])
    last_name = StringField('Last name', validators=[InputRequired()])
    email = EmailField('Email', validators=[InputRequired(), Email()])
    photo = FileField('Profile Image', validators=[Optional(), FileAllowed(images, 'Images only!')])
    area_code = StringField('Phone area code only', validators=[InputRequired(), Length(1, 6)])
    mobile_phone = IntegerField('Phone numbers only', validators=[InputRequired(), Unique(User.mobile_phone)])
    city = StringField('City', validators=[InputRequired()])
    recruiter = SelectField('Recruiter', validators=[Optional()])
    custom_profession = StringField('Custom Profession', validators=[Optional()])
    state = SelectField(u'Select US State', choices=states)
    profession = SelectField(u'Profession', choices=professions)

    gender = SelectField(u'Gender', choices=[('Male', 'Male'), ('Female', 'Female'), ('Transgender', 'Transgender')])
    zip = StringField('Zip Code', validators=[InputRequired(), Length(1, 7)])
    country = SelectField(u'Select Country', choices=countries)
    #summary_text = TextAreaField('Summary Text or Description')
    password = PasswordField(
        'Password',
        validators=[
            InputRequired(),
            EqualTo('password2', 'Passwords must match')
        ])
    password2 = PasswordField('Confirm password', validators=[InputRequired()])
    submit = SubmitField('Register')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered. (Did you mean to '
                                  '<a href="{}">log in</a> instead?)'.format(url_for('account.login')))


class RequestResetPasswordForm(FlaskForm):
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    submit = SubmitField('Reset password')

    # We don't validate the email address so we don't confirm to attackers
    # that an account with the given email exists.


class ResetPasswordForm(FlaskForm):
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    new_password = PasswordField(
        'New password',
        validators=[
            InputRequired(),
            EqualTo('new_password2', 'Passwords must match.')
        ])
    new_password2 = PasswordField(
        'Confirm new password', validators=[InputRequired()])
    submit = SubmitField('Reset password')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first() is None:
            raise ValidationError('Unknown email address.')


class CreatePasswordForm(FlaskForm):
    password = PasswordField(
        'Password',
        validators=[
            InputRequired(),
            EqualTo('password2', 'Passwords must match.')
        ])
    password2 = PasswordField(
        'Confirm new password', validators=[InputRequired()])
    submit = SubmitField('Set password')


class ChangePasswordForm(FlaskForm):
    old_password = PasswordField('Old password', validators=[InputRequired()])
    new_password = PasswordField(
        'New password',
        validators=[
            InputRequired(),
            EqualTo('new_password2', 'Passwords must match.')
        ])
    new_password2 = PasswordField(
        'Confirm new password', validators=[InputRequired()])
    submit = SubmitField('Update password')


class ChangeEmailForm(FlaskForm):
    email = EmailField(
        'New email', validators=[InputRequired(),
                                 Length(1, 64),
                                 Email()])
    password = PasswordField('Password', validators=[InputRequired()])
    submit = SubmitField('Update email')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')


class ChangeProfileForm(BaseModelForm):
    first_name = StringField('First name', validators=[InputRequired()])
    last_name = StringField('Last name', validators=[InputRequired()])
    summary_text = TextAreaField('Summary Text or Description')
    photo = FileField('Profile Image', validators=[Optional(), FileAllowed(images, 'Images only!')])
    area_code = StringField('Phone area code only', validators=[InputRequired()])
    mobile_phone = IntegerField('Phone numbers only', validators=[InputRequired(), Unique(User.mobile_phone)])
    city = StringField('City', validators=[InputRequired()])
    state = SelectField(u'Select US State', choices=states)

    profession = SelectField(u'Profession', choices=professions)
    custom_profession = StringField('Custom Profession', validators=[Optional()])
    recruiter = SelectField('Recruiter', validators=[Optional()])
    gender = SelectField(u'Gender', choices=[('Male', 'Male'), ('Female', 'Female'), ('Transgender', 'Transgender')])
    zip = StringField('Zip Code', validators=[InputRequired(), Length(1, 7)])
    country = SelectField(u'Select Country', choices=countries)

    submit = SubmitField('Update')


class ContactForm(FlaskForm):
    text = TextAreaField('Message', validators=[InputRequired()])
    submit = SubmitField('Send')


class AddProfileForm(FlaskForm):
    first_name = StringField('First name', validators=[InputRequired()])
    last_name = StringField('Last name', validators=[InputRequired()])
    title = StringField('Current or Desired Job Title', validators=[InputRequired()])
    header = TextAreaField('Profile Summary', validators=[InputRequired()])
    commitment = SelectField(u'Current or future commitment', choices=[('Full Time', 'Full Time'), ('Part Time', 'Part Time')])
    type_of_work = SelectField(u'Type Of Work', choices=[('Employee', 'Employee'), ('Contractor', 'Contractor')])
    image = FileField('Profile Image', validators=[InputRequired(), FileAllowed(images, 'Images only!')])
    cover = FileField('Profile Cover Image', validators=[InputRequired(), FileAllowed(images, 'Images only!')])
    submit = SubmitField('Send')


# Please reflect any changes to this form in account/profile_edit.html template
class AddProfileSkillForm(FlaskForm):
    name = StringField(u'Title', validators=[InputRequired()])
    description = StringField(u'Description', validators=[InputRequired()])
    exp = SelectField(u'Experience', choices=[('Newbie', 'Newbie'), ('Intermediate', 'Intermediate'), ('Experienced', 'Experienced'), ('Guru', 'Guru')])
    submit = SubmitField('+')


# Please reflect any changes to this form in account/profile_edit.html template
class AddProfileEduForm(FlaskForm):
    school = StringField(u'School', validators=[InputRequired()])
    degree = SelectField(u'Degree', choices=[('Associate', 'Associate'), ('Bachelor', 'Bachelor'), ('Master', 'Master'), ('Doctoral', 'Doctoral')])
    start_date = DateField('Start', validators=[InputRequired()])
    end_date = DateField('End', validators=[Optional()])
    submit = SubmitField('Send')


# Please reflect any changes to this form in account/profile_edit.html template
class AddProfileJobForm(FlaskForm):
    title = StringField('Job Title', validators=[InputRequired()])
    company = StringField('Company', validators=[InputRequired()])
    start_date = DateField('Start', validators=[InputRequired()])
    end_date = DateField('End', validators=[Optional()])
    commitment = SelectField(u'Commitment', choices=[('Full Time', 'Full Time'), ('Part Time', 'Part Time')])
    submit = SubmitField('Send')


# Please reflect any changes to this form in account/profile_edit.html template
class AddProfileLangForm(FlaskForm):
    #(name='commitment', B='Beginner', E='Elementary', I='Intermediate', U='Upper Intermediate', A='Advanced', P='Proficient'))
    level = SelectField(u'Level', choices=[('Beginner', 'Beginner'), ('Elementary', 'Elementary'), ('Intermediate', 'Intermediate'),
                                            ('Upper Intermediate', 'Upper Intermediate'), ('Advanced', 'Advanced'), ('Proficient', 'Proficient')])
    lang = SelectField("Language", choices=get_langs())
    submit = SubmitField('Send')


# Please reflect any changes to this form in account/profile_edit.html template
class AddProfileProjectForm(FlaskForm):
    name = StringField(u'Project Name', validators=[InputRequired()])
    description = StringField(u'Description', validators=[InputRequired()])
    start_date = DateField('Start', validators=[InputRequired()])
    end_date = DateField('End', validators=[Optional()])
    submit = SubmitField('Send')
