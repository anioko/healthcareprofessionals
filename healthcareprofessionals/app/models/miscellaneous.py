import whoosh
from app.libs.flask_whooshee import AbstractWhoosheer

from app import whooshee
from .user import Question, User, Job
from .marketplace import MProduct
from app.utils import db


class EditableHTML(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    editor_name = db.Column(db.String(100), unique=True)
    value = db.Column(db.Text)

    @staticmethod
    def get_editable_html(editor_name):
        editable_html_obj = EditableHTML.query.filter_by(
            editor_name=editor_name).first()

        if editable_html_obj is None:
            editable_html_obj = EditableHTML(editor_name=editor_name, value='')
        return editable_html_obj

    @property
    def serialize(self):
        return {
            'id': self.id,
            'editor_name': self.editor_name,
            'value': self.value
        }
