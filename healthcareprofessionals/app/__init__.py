import operator
import os
import uuid
from hashlib import sha512

from flask import Flask, session, request
from flask_assets import Environment
from flask_ckeditor import CKEditor
from flask_compress import Compress
from flask_jwt_extended import JWTManager
from flask_login._compat import text_type
from flask_login.utils import _get_remote_addr
from flask_mail import Mail
from flask_moment import Moment
from flask_recaptcha import ReCaptcha
from flask_rq import RQ
from flask_session import Session
from flask_share import Share
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask_wtf import CSRFProtect

from app.libs.flask_whooshee import Whooshee
from app.utils import db, login_manager, get_cart, image_size, json_load
from config import config
from .assets import app_css, app_js, vendor_css, vendor_js

# from app.blueprints.api.views import main_api

# from app.models import Notification

basedir = os.path.abspath(os.path.dirname(__file__))

mail = Mail()
csrf = CSRFProtect()
compress = Compress()
images = UploadSet('images', IMAGES)
docs = UploadSet('docs', ('rtf', 'odf', 'ods', 'gnumeric', 'abw', 'doc', 'docx', 'xls', 'xlsx', 'pdf'))
share = Share()
moment = Moment()
jwt = JWTManager()
sess = Session()
# Set up Flask-Login
login_manager.session_protection = 'strong'
login_manager.login_view = 'account.login'

# import app.models as models

whooshee = Whooshee()
recaptcha = ReCaptcha()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    # Set up extensions
    mail.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    csrf.init_app(app)
    compress.init_app(app)
    RQ(app)
    configure_uploads(app, images)
    configure_uploads(app, docs)
    CKEditor(app)
    share.init_app(app)
    moment.init_app(app)
    jwt.init_app(app)
    sess.init_app(app)
    # Register Jinja template functions
    from .utils import register_template_utils
    register_template_utils(app)

    # Set up asset pipeline
    assets_env = Environment(app)
    dirs = ['assets/styles', 'assets/scripts']
    for path in dirs:
        assets_env.append_path(os.path.join(basedir, path))
    assets_env.url_expire = True

    assets_env.register('app_css', app_css)
    assets_env.register('app_js', app_js)
    assets_env.register('vendor_css', vendor_css)
    assets_env.register('vendor_js', vendor_js)

    # Configure SSL if platform supports it
    if not app.debug and not app.testing and not app.config['SSL_DISABLE']:
        from flask_sslify import SSLify
        SSLify(app)

    # Create app blueprints
    from .blueprints.public import public as public_blueprint
    app.register_blueprint(public_blueprint)

    from .blueprints.seo_arizona import seo_arizona as seo_arizona_blueprint
    app.register_blueprint(seo_arizona_blueprint)

    from .blueprints.seo_world import seo_world as seo_world_blueprint
    app.register_blueprint(seo_world_blueprint)

    from .blueprints.main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .blueprints.account import account as account_blueprint
    app.register_blueprint(account_blueprint, url_prefix='/account')

    from .blueprints.admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .blueprints.marketplace import marketplace as marketplace_blueprint
    app.register_blueprint(marketplace_blueprint, url_prefix='/marketplace')

    from .blueprints.jobs import jobs as jobs_blueprint
    app.register_blueprint(jobs_blueprint, url_prefix='/jobs')

    from .blueprints.crawlers import crawlers as crawlers_blueprint
    app.register_blueprint(crawlers_blueprint)

    from .blueprints.promos import promos as promos_blueprint
    app.register_blueprint(promos_blueprint, url_prefix='/promos')

    from .blueprints.posts import post_blueprint as post_blueprint
    app.register_blueprint(post_blueprint)

    from .blueprints.organisations import organisations as organisations_blueprint
    app.register_blueprint(organisations_blueprint, url_prefix='/organisations')

    from .blueprints.sitemaps import sitemaps as sitemaps_blueprint
    app.register_blueprint(sitemaps_blueprint)

    from .blueprints.api import api as apis_blueprint
    app.register_blueprint(apis_blueprint, url_prefix='/api')

    from .blueprints.blog import blog
    app.register_blueprint(blog, url_prefix='/blog')

    from .blueprints.employer import employer
    app.register_blueprint(employer, url_prefix='/employer')

    @app.before_request
    def before_request():
        try:
            session['cart_id']
        except:
            u = uuid.uuid4()
            user_agent = request.headers.get('User-Agent')
            if user_agent is not None:
                user_agent = user_agent.encode('utf-8')
            base = 'cart: {0}|{1}|{2}'.format(_get_remote_addr(), user_agent, u)
            if str is bytes:
                base = text_type(base, 'utf-8', errors='replace')  # pragma: no cover
            h = sha512()
            h.update(base.encode('utf8'))
            session['cart_id'] = h.hexdigest()

    @app.cli.command()
    def reindex():
        with app.app_context():
            whooshee.reindex()

    @app.cli.command()
    def routes():
        rules = []
        for rule in app.url_map.iter_rules():
            subdomain = "no subdomain" if not rule.subdomain else rule.subdomain
            methods = ','.join(sorted(rule.methods))
            rules.append((rule.endpoint, methods, str(rule), subdomain))

        sort_by_rule = operator.itemgetter(2)
        for endpoint, methods, rule, subdomain in sorted(rules, key=sort_by_rule):
            route = '{:25s} {:60s} {:25s} {}'.format(subdomain, endpoint, methods, rule)
            print(route)

    whooshee.init_app(app)
    recaptcha.init_app(app)

    return app
